# mazda-aux-mod

Revival of the now defunct Sylfex aux mod.
This will add an aux jack to your mazda.
It should be compatible with all oem fitted radios with an "MD/Tape" button.
If a MiniDisc or Tape module is fitted, remove it before installing this Mod.


## Features

* Allows the radio to switch into tape / minidisc mode and accept line level audio through a 3.5mm jack.
* Is completely switched off and draws no power, when the car is off.
* Isolated Auxillary USB charging port which allows using something like a Fiio BTR3k in car mode.


## Installation

1. Check that your radio is compatible: It should have a MD/Tape button and depending on the model, a slot for a tape / minidisc addon.
2. TODO


## Structure

I compiled some references while searching the web for info. Find them in the reference folder.

